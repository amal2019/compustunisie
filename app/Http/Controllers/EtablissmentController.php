<?php

namespace App\Http\Controllers;

use App\Models\Etablissment;
use Illuminate\Http\Request;

class EtablissmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Etablissment  $etablissment
     * @return \Illuminate\Http\Response
     */
    public function show(Etablissment $etablissment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Etablissment  $etablissment
     * @return \Illuminate\Http\Response
     */
    public function edit(Etablissment $etablissment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Etablissment  $etablissment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etablissment $etablissment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Etablissment  $etablissment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etablissment $etablissment)
    {
        //
    }
}
