<?php

namespace App\Http\Controllers;

use App\Models\CentreFormation;
use Illuminate\Http\Request;

class CentreFormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CentreFormation  $centreFormation
     * @return \Illuminate\Http\Response
     */
    public function show(CentreFormation $centreFormation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CentreFormation  $centreFormation
     * @return \Illuminate\Http\Response
     */
    public function edit(CentreFormation $centreFormation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CentreFormation  $centreFormation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CentreFormation $centreFormation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CentreFormation  $centreFormation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CentreFormation $centreFormation)
    {
        //
    }
}
