@extends('layouts1.master')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Ajouter un étudiant</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('etudiants.create') }}"> Créer un nouveau produit</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>CIN</th>
            <th>Email</th>
            
            <th width="280px">Action</th>
        </tr>
        @foreach ($etudiants as $etudiant)
        <tr>
           
            <td>{{ $etudiant->nom }}</td>
            <td>{{ $etudiant->prenom }}</td>
            <td>{{ $etudiant->cin }}</td>
            <td>{{ $etudiant->email }}</td>
            
            <td>
                <form action="{{ route('etudiants.destroy',$etudiant->nom) }}" method="POST">
   
                    <a class="btn btn-outline-primary" href="{{ route('etudiants.show',$etudiant->nom) }}">Montrer</a>
    
                    <a class="btn btn-outline-success" href="{{ route('etudiants.edit',$etudiant->prenom) }}">Éditer</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-outline-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div class="d-flex justify-content-center pagination-lg">
    {!! $etudiants->links('pagination::bootstrap-4') !!}
      </div>
@endsection