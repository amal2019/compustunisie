@extends('layouts1.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Modifier étudiant</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('etudiants.index') }}">Liste étudiant</a></li>
              <li class="breadcrumb-item active">Etudiant</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <form action="{{ route('etudiants.update',$etudiant->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Nom</label>
                <input type="text" id="inputName" name="name"  value="{{ $etudiant->nom }}" placeholder="name" class="form-control">
                @error('name')
                <div class="alert alert-danger">eb
                  {{$message}}
                </div>
                @enderror
            </div>
              <div class="form-group">
                <label for="inputDescription">Prénom</label>
                <input id="inputDescription" name="short_description"  value="{{ $etudiant->prenom }}" placeholder="short_description" class="form-control" rows="4">{{ $etudiant->short_description }}</textarea>
                @error('short_description')
                <div class="alert alert-danger">
                  {{$message}}
                </div>
                @enderror
            </div>

              <div class="form-group">
                <label for="inputClientCompany">CIN</label>
                <input type="text" id="inputClientCompany" name="price"   value="{{ $etudiant->cin }}" placeholder="price" class="form-control">
                @error('price')
                <div class="alert alert-danger">
                  {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="inputClientCompany">Email</label>
                <input type="text" id="inputClientCompany" name="sale_price"   value="{{ $etudiant->email}}" placeholder="sale_price" class="form-control">
                @error('sale_price')
                <div class="alert alert-danger">
                  {{$message}}
                </div>
                @enderror
            </div>
           
           


            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Modifier" class="btn btn-success float-right">
        </div>
      </div>
    </form>
    </section>
    <!-- /.content -->
  </div>
@endsection
