<?php

namespace Database\Factories;

use App\Models\Etablissment;
use Illuminate\Database\Eloquent\Factories\Factory;

class EtablissmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Etablissment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
