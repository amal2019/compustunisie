<?php

namespace Database\Factories;

use App\Models\PosteTravail;
use Illuminate\Database\Eloquent\Factories\Factory;

class PosteTravailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PosteTravail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
