<?php

namespace Database\Factories;

use App\Models\CentreFormation;
use Illuminate\Database\Eloquent\Factories\Factory;

class CentreFormationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CentreFormation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
